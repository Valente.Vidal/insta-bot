from selenium.webdriver import Chrome
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains

from bs4 import BeautifulSoup
import time as t
import random
from datetime import datetime, time
import os
import pickle
from secrets import *
import argparse
from progress.bar import ShadyBar
from sys import platform

#if platform == "linux":
from image_recon import *

ROOT_PATH = os.path.dirname(os.path.realpath(__file__))







class instabot:

    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.browser = self.gen_browser()


    def random_wait_time(self):
        return random.randint(2,4)

    def random_recent_post(self):
        return random.randint(9,15)

    def random_wait_time_between_actions(self):
        return random.randint(3,9)

    def gen_browser(self):
        chrome_options = webdriver.ChromeOptions()
        if platform == "linux":
            chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--user-agent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36"')
        
        browser = webdriver.Chrome(executable_path=os.path.join(ROOT_PATH, "chromedriver"), chrome_options=chrome_options)
        return browser

    def login(self):
        print("Logging In...")
        login_url = "https://www.instagram.com/accounts/login/"
        self.browser.get(login_url)
        t.sleep(4)
        self.browser.find_element_by_xpath("//input[@name='username']").send_keys(self.username)
        t.sleep(self.random_wait_time())
        self.browser.find_element_by_xpath("//input[@name='password']").send_keys(self.password)
        t.sleep(self.random_wait_time())
        self.browser.find_element_by_xpath("//*[@id='react-root']/section/main/div/article/div/div[1]/div/form/div[4]/button").click()
        t.sleep(self.random_wait_time())



    def post_by_username(self, username):
        post_by_username = []
        post_link = "https://www.instagram.com/p/"

        url = "https://www.instagram.com/" + username
        self.browser.get(url)
        t.sleep(self.random_wait_time())
        links = self.browser.find_elements_by_tag_name('a')
        for link in links:
            if post_link in link.get_attribute('href'):
                #print (link.get_attribute('href'))
                post_by_username.append(link.get_attribute('href'))
        return post_by_username

        
    def posts_by_hashtag(self, hashtag):
        posts_by_hashtag = []
        post_link = "https://www.instagram.com/p/"

        search_hashtag_url = f"https://www.instagram.com/explore/tags/{hashtag}/"
        self.browser.get(search_hashtag_url)
        t.sleep(self.random_wait_time())
        links = self.browser.find_elements_by_tag_name('a')
        for link in links:
            if post_link in link.get_attribute('href'):
                #print (link.get_attribute('href'))
                posts_by_hashtag.append(link.get_attribute('href'))
        return posts_by_hashtag

    def heart_post(self, post_url):
        self.browser.get(post_url)
        t.sleep(self.random_wait_time_between_actions())
        self.browser.find_element_by_xpath("//*[@id='react-root']/section/main/div/div/article/div[2]/section[1]/span[1]/button/span").click()
                                                   

    def schedule(self):
        start_time = time(2)
        stop_time = time(23)
        if datetime.now().time() > start_time and datetime.now().time() < stop_time :
            return True
        else:
            pass

    def save_cookies(self):
        print("Saving cookies")
        cookie_name = f"cookies_{self.username}.pkl"
        pickle.dump( self.browser.get_cookies() , open(cookie_name,"wb"))

    def load_cookies(self):
        try:
            cookie_name = f"cookies_{self.username}.pkl"
            cookies = pickle.load(open(cookie_name, "rb"))
            for cookie in cookies:
                if 'expiry' in cookie:
                    del cookie['expiry']
                self.browser.add_cookie(cookie)
        except FileNotFoundError:
            print("No Cookies Found")

    def check_if_logged_in(self):
        print("Checking if logged in...")
        self.browser.get("https://www.instagram.com/accounts/login/?hl=en") 
        t.sleep(2)
        self.load_cookies()
        t.sleep(2)
        self.browser.get("https://www.instagram.com/accounts/login/?hl=en")
        if "Don't have an account?" in self.browser.find_element_by_tag_name('body').text:
            self.login()
            self.save_cookies()
        else:
            print ("Logged in!")

    def get_posts_username(self, post):
        self.browser.get(post)
        username = self.browser.find_element_by_xpath("//*[@id='react-root']/section/main/div/div/article/header/div[2]/div[1]/div[1]/h2/a").text
        return username


    def progress_bar(self, timeA, timeB): #Time Wait in seconds
        rantime = random.randint(timeA, timeB)
        bar = ShadyBar('Waiting Before Continuing', max=rantime)
        for i in range(rantime):
            t.sleep(1)
            bar.next()
        bar.finish()


    def download_image(self, post):
        self.browser.get(post)
        print("Saving Post Image")        
        filename = f"post_image_{self.username}.png" 
        if os.path.exists(filename):
            os.remove(filename)
        with open(filename, 'wb') as img_file:
            img_file.write(self.browser.find_element_by_xpath("//*[@id='react-root']/section/main/div/div/article/div[1]/div/div/div[1]/img").screenshot_as_png)

    def write_comment(self, post, comment):
        self.browser.get(post)
        try:

            self.browser.implicitly_wait(10)
            self.browser.find_element_by_xpath("//*[@id='react-root']/section/main/div/div/article/div[2]/section[3]/div/form/textarea").click()   
            self.browser.find_element_by_xpath("//*[@id='react-root']/section/main/div/div/article/div[2]/section[3]/div/form/textarea").send_keys(comment)
            self.browser.implicitly_wait(5)                                             
            print("clicking 'post'")
            self.browser.find_element_by_xpath("//*[@id='react-root']/section/main/div/div/article/div[2]/section[3]/div/form/button").click()
        except Exception as e:
            print (e)



    def coordinate_random(self):
        self.check_if_logged_in()
      
        hashtags = open(os.path.join(ROOT_PATH, f"hashtags_{self.username}.txt"), "r").read().splitlines()
                
        n = 0
        while True:
            if self.schedule() == True:
                n = n + 1
                print ("On Cycle Number: " + str(n))
                print (datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                hashtag = random.choice(hashtags)
                try:
                    print (f"Hearting a recent post for: {hashtag}")
                    posts_by_hashtag_list = self.posts_by_hashtag(hashtag)
                    post = posts_by_hashtag_list[self.random_recent_post()]
                    print ("url: ", post)
                    username = self.get_posts_username(post)
                    print ("username: ", username)

                    try: 
                        self.heart_post(post)    
                        #if platform == "linux":
                        self.download_image(post)
                        filename = f"post_image_{self.username}.png"
                        prediction = image_recon(filename)
                        self.write_comment(post, f"That's an interesting {prediction}!")
                    except Exception as e:
                        print (e)


                    print ("Waiting some time before continuing... ")
                    self.progress_bar(540,660)
                except Exception as e:
                    print (e)
                    print ("Oopss Error.... Waiting sometime...")
                    self.progress_bar(180,300)
                    
            else:
                print("Sleeping ZZZzzz...")
                print (datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                self.progress_bar(540,660)

        self.browser.close()



    def posts_by_user(self, user):
        posts_by_user = []
        post_link = "https://www.instagram.com/p/"

        user_insta = "https://www.instagram.com/" + user


        self.browser.get(user_insta)
        t.sleep(self.random_wait_time())
        links = self.browser.find_elements_by_tag_name('a')
        for link in links:
            if post_link in link.get_attribute('href'):
                #print (link.get_attribute('href'))
                posts_by_user.append(link.get_attribute('href'))
        return posts_by_user


    def coordinate_by_user(self, target):
        self.check_if_logged_in()
        print((target))



        posts_by_user = self.posts_by_user(target)
        print (f"Got {(len(posts_by_user))} posts")
        for post in posts_by_user:
            print (datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
            print ("url: ", post)
                    

            self.heart_post(post)
            if platform == "linux":
                self.download_image(post)
                filename = f"post_image_{self.username}.png"
                prediction = image_recon(filename)
                self.write_comment(post, f"Wow that a cool {prediction}")
            print ("Waiting some time before continuing... ")
            self.progress_bar(15,30)
                
        


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='A Simple Instabot!')
    parser.add_argument("--user", help="Add your instagram handle")
    parser.add_argument("--password", help="Add your instagram password")
    parser.add_argument("--mode", help="Choose a mode random or target")
    parser.add_argument("--target", help="Enter targets username")
    args = parser.parse_args()


    if args.mode == "target":
        print(f"Stalking: {args.target}")
        a = instabot(args.user, args.password)
        a.coordinate_by_user(args.target)
    
    elif args.mode == "random":
        print ("Getting that exposure!!!!")
        a = instabot(args.user, args.password)
        a.coordinate_random()
        